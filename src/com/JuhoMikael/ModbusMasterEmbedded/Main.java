package com.JuhoMikael.ModbusMasterEmbedded;

import java.net.InetAddress;

import eu.hansolo.enzo.gauge.FlatGauge;
import eu.hansolo.enzo.gauge.FlatGaugeBuilder;
import eu.hansolo.enzo.gauge.Gauge;
import eu.hansolo.enzo.led.Led;
import eu.hansolo.enzo.led.LedBuilder;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import radialMenu.RadialMenu;
import radialMenu.RadialMenuBuilder;
import radialMenu.RadialMenuItemBuilder;
import radialMenu.RadialMenuOptionsBuilder;
import symbols.SymbolType;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;


public class Main extends Application {
	
	Boolean fullScreenToggle = true;
	int height = 600;
	int width = 1024;
	
	private Stage primaryStage;
	Scene scene1,scene2;
	BorderPane border = new BorderPane();
	Label head = new Label("ModBusFXMasterV1");
	

	Button bbutton2 = new Button("DI");
	VBox vbox = new VBox(3);
	Button button = new Button("Hello1");
	Button button2 = new Button("Hola2");
	FlatGauge gauge11 = new FlatGauge();
	FlatGauge gauge22 = new FlatGauge();
	Led led11 = new Led();
	Button ss = new Button("Submit Settings");
	modbusMasterEngine engine = new modbusMasterEngine();
	double t =0;
	double h = 0;
	boolean di = false;
	boolean dii = false;
	boolean diii= false;
	TextField f41 = new TextField("");
	TextField f42 = new TextField("");
	TextArea ta = new TextArea();
	toggleiOSButton ibutton = new toggleiOSButton();
	toggleiOSButton ibutton2 = new toggleiOSButton();
	toggleiOSButton ibutton3 = new toggleiOSButton();
	private AnimationTimer timer;
	private long lastTimerCall;
	
	@Override
	public void init(){
		
		lastTimerCall = System.nanoTime() + 1_000_000_000l;
		
		timer = new AnimationTimer(){
			@Override public void handle(long now){
				if(now > lastTimerCall + 2_000_000_000l){
					gauge11.setValue(t);
					gauge22.setValue(h);
					gauge11.setBarColor(Color.BLUEVIOLET);
					gauge22.setBarColor(Color.BLUEVIOLET);
					led11.setOn(di);
					lastTimerCall=now;
				}
				
				
			}
		};
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setFullScreen(fullScreenToggle);
			Scene scene = new Scene(border,width,height);
			vbox.getChildren().addAll(bbutton2);
			StackPane headHolder = new StackPane();
			headHolder.getChildren().add(head);
			border.setTop(headHolder);
			headHolder.setAlignment(Pos.CENTER);
			headHolder.setPadding(new Insets(30,0,0,0));
			border.setBackground(new Background(new BackgroundFill(Color.ALICEBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
			StackPane hold = new StackPane();
			BorderPane bp = new BorderPane();
			hold = getMainMenu();
			bp.setBottom(hold);
			
			border.setLeft(bp);
			hold.setAlignment(Pos.BOTTOM_CENTER);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
			timer.start();
			engine.start();
			
			/* new Thread(new Runnable() {
	    	    @Override public void run() {
	    	            while(true){
	    	          try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
	    	          
	    	           t = (double) engine.getIR1();
	    	           h = (double) engine.getIR2();
	    	           di=  engine.getDI1();
	    	           System.out.println(engine.getIR1());

	    	    	     engine.setDO1(ibutton.toggle.switchedOnProperty().get());
	    	    	     engine.setDO2(ibutton2.toggle.switchedOnProperty().get());

	    	            System.out.println(t+"-"+h);

	    	          System.out.println("MITTAREILLEMENEVÄTARVOT: "+t+"+"+h);
	    	            }
	    	        }
	    	    
	    	}).start(); */
			
			
			
			ss.setOnAction(e -> {System.out.println("RTU IP: " + f41.getText() + "ALARM LIMIT:" + f42.getText());});
			
			bbutton2.setOnAction(e -> {
				border.setCenter(getDI());
			});
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public StackPane getMeter(){
		
		final FlatGauge gauge1 = FlatGaugeBuilder.create()
                .prefSize(421, 421)
                .title("Temp RTU1")
                .unit("°C")
                .minValue(0)
                .maxValue(100)
                .backgroundColor(Color.TRANSPARENT)
                .barColor(Color.BLUEVIOLET)
                .animationDuration(2000)
                .build();
		
		final FlatGauge gauge2 = FlatGaugeBuilder.create()
                .prefSize(421, 421)
                .title("Temp RTU2")
                .unit("°C")
                .minValue(0)
                .maxValue(100)
                .backgroundColor(Color.CORAL)
                .barColor(Color.BLUEVIOLET)
                .animationDuration(2000)
                .build();
		
		
		gauge11 =gauge1;
		gauge22 = gauge2;
		
		GridPane pane21 = new GridPane();
		pane21.setPadding(new Insets(5,5,10,5));
		pane21.setHgap(10); 
        pane21.setVgap(10); 
        pane21.add(gauge11, 0, 0); 
        pane21.add(gauge22, 1, 0);
        gauge22.setPadding(new Insets(0,0,0,30));

        pane21.setPadding(new Insets(200,2,10,2));
        
		
		
		return new StackPane(pane21);
	}
	
	public StackPane getDI(){
		
		StackPane pane3 = new StackPane();
		
		final Led led1                = LedBuilder.create()
                .ledType(Led.LedType.ROUND)
                .ledColor(Color.BLUEVIOLET)
                .prefWidth(100)
                .prefHeight(100)
               
                .build();
		
		final Led led2                = LedBuilder.create()
                .ledType(Led.LedType.ROUND)
                .ledColor(Color.BLUEVIOLET)
                .prefWidth(100)
                .prefHeight(100)
                .build();
		
		final Led led3                = LedBuilder.create()
                .ledType(Led.LedType.ROUND)
                .ledColor(Color.BLUEVIOLET)
                .prefWidth(100)
                .prefHeight(100)
                .build();
		
		led11 = led1;

		Label DIL1= new Label("DI1");

		HBox hboxli = new HBox(2);
		HBox hboxlii = new HBox(2);
		HBox hboxliii = new HBox(2);
		
		hboxli.getChildren().addAll(led11,DIL1);
		DIL1.setPadding(new Insets(70,10,70,30));

		VBox pane11 = new VBox();
		pane11.getChildren().addAll(hboxli,hboxlii,hboxliii);
		pane3.getChildren().addAll(pane11);
		pane3.setPadding(new Insets(150,10,10,10));
		
		return new StackPane(pane3);
	}
	
	public VBox getSettings(){
		
		VBox pane4 = new VBox();
		HBox pane41 = new HBox();
		HBox pane42 = new HBox();
		Label l4 = new Label("Settings:");
		Text l41 = new Text("RTU IP Address:");
		Text l42 = new Text("Humidity Alarm Treshold (%):");
		
		l4.setPadding(new Insets(10,10,30,10));
		pane41.getChildren().addAll(l41,f41);
		pane42.getChildren().addAll(l42,f42);
		pane4.getChildren().addAll(l4,pane41,pane42,ss);
		
		ss.setOnAction(e -> {
			try {
				engine.setSlaveInet(InetAddress.getByName("192.168.11.73"));
				engine.start();
				f41.setText("192.168.11.73");
				f41.setEditable(false);
				ss.setVisible(false);
				
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("Something went seriously wrong");
			}
			
		});
		
		pane4.setPadding(new Insets(150,10,10,10));
		
		return new VBox(pane4);
	}
	
	public StackPane getAlarms(){
		
		VBox pane5 = new VBox();
		
		Label tal = new Label("Alarms: ");
		pane5.getChildren().addAll(tal,ta);
		ta.setPrefSize(500, 500);
		ta.setText("Alaaarm!!");
		pane5.setPadding(new Insets(100,10,10,10));
		
		return new StackPane(pane5);
	}
	
	public StackPane getDO(){
		
		VBox pane6 = new VBox();
		HBox pane51 = new HBox();
		HBox pane52 = new HBox();
		HBox pane53 = new HBox();
		
		Label DO1 = new Label("DO1");
		Label DO2 = new Label("DO2");
		//Label DO3 = new Label("DO3");
		
		pane51.getChildren().addAll(ibutton.createiOSToggle(),DO1);
		pane52.getChildren().addAll(ibutton2.createiOSToggle(),DO2);
		//pane53.getChildren().addAll(ibutton3.createiOSToggle(),DO3);
		
		DO1.setPadding(new Insets(100,10,0,-10));
		DO2.setPadding(new Insets(100,10,0,-10));
		//DO3.setPadding(new Insets(100,10,0,-10));
		
		pane6.getChildren().addAll(pane51,pane52,pane53);
		
		return new StackPane(pane6);
	}
	
	public StackPane getMainMenu(){
		
		StackPane pane0 = new StackPane();
		
		
		final RadialMenu radialMenu = RadialMenuBuilder.create()
	            .options(RadialMenuOptionsBuilder.create()
	                             .degrees(180)
	                             .offset(-120)
	                             .radius(200)
	                             .buttonSize(100)
	                             .buttonHideOnSelect(false)
	                             .buttonHideOnClose(false)
	                             .buttonAlpha(1.0)
	                             .simpleMode(true)
	                             .strokeVisible(false)
	                             .buttonForegroundColor(Color.BLUEVIOLET)
	                             .buttonFillColor(Color.BLUEVIOLET)
	                             .buttonStrokeColor(Color.BLUEVIOLET)
	                             
	                             .build()
	                             
	            		)
	            				
	            .items(
	            			  //RadialMenuItemBuilder.create().symbol(SymbolType.SETTINGS).tooltip("Clock").size(70).build(),
	            			  //RadialMenuItemBuilder.create().symbol(SymbolType.ALARM).tooltip("Clock").size(70).build(),
	                          RadialMenuItemBuilder.create().symbol(SymbolType.BOOLEAN).tooltip("Digital In").size(70).build(),
	                          RadialMenuItemBuilder.create().symbol(SymbolType.GAUGE).tooltip("Temperature").size(70).build(),
	                          RadialMenuItemBuilder.create().symbol(SymbolType.MULTI_RELAY).tooltip("Control").size(70).build(),
	                          RadialMenuItemBuilder.create().symbol(SymbolType.DELETE).tooltip("Exit").size(70).build()
	                          )
	                          
	            .build();
	radialMenu.setPrefSize(500, 500);
	radialMenu.setOnItemSelected(selectionEvent -> System.out.println("item " + selectionEvent.item.getTooltip() + " selected"));
	radialMenu.setOnItemClicked(clickEvent -> {System.out.println("item " + clickEvent.item.getTooltip() + " clicked");
		
	if(clickEvent.item.getSymbolType().equals(SymbolType.GAUGE)){
		border.setCenter(getMeter());
		
		gauge22.setBarColor(Color.BLUEVIOLET);
		gauge11.setBarColor(Color.BLUEVIOLET);
	}
		
		else if(clickEvent.item.getSymbolType().equals(SymbolType.BOOLEAN)){
			//primaryStage.setScene(scene01);
			StackPane hold2 = new StackPane();
			hold2 = getDI();
			border.setCenter(hold2);
			hold2.setAlignment(Pos.BOTTOM_CENTER);
		}
	
		else if(clickEvent.item.getSymbolType().equals(SymbolType.DELETE)){
			System.exit(0);
		}
	
	/*	else if(clickEvent.item.getSymbolType().equals(SymbolType.SETTINGS)){
			border.setCenter(getSettings());
		} 
	
		else if(clickEvent.item.getSymbolType().equals(SymbolType.ALARM)){
			border.setCenter(getAlarms());
		}*/
		
		else if(clickEvent.item.getSymbolType().equals(SymbolType.MULTI_RELAY)){
			border.setCenter(getDO());
			
		}
		
		else
			System.out.println("choose again");
	
	});
	
	pane0.getChildren().add(radialMenu);
	
	pane0.setPadding(new Insets(100,10,-10,-80));
		
		return new StackPane(pane0);
	}
	
}
