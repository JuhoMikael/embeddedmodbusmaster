package com.JuhoMikael.ModbusMasterEmbedded;

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import javafx.util.Duration;

public class toggleiOSButton  {

	public ToggleSwitch toggle = new ToggleSwitch();
	
    public Pane createiOSToggle() {
        Pane root = new Pane();
        root.setPrefSize(300, 300);

        Rectangle bg = new Rectangle(300, 300);

        
        toggle.setTranslateX(100);
        toggle.setTranslateY(100);

       
        root.getChildren().addAll(toggle);
        return root;
    }

    public class ToggleSwitch extends Parent {

        private BooleanProperty switchedOn = new SimpleBooleanProperty(false);

        private TranslateTransition translateAnimation = new TranslateTransition(Duration.seconds(0.25));
        private TranslateTransition translateAnimation2 = new TranslateTransition(Duration.seconds(0.25));
        private FillTransition fillAnimation = new FillTransition(Duration.seconds(0.25));

        private ParallelTransition animation = new ParallelTransition(translateAnimation, fillAnimation, translateAnimation2);

        public BooleanProperty switchedOnProperty() {
            return switchedOn;
        }

        public ToggleSwitch() {
            
        	Rectangle background = new Rectangle(100, 50);
            background.setArcWidth(50);
            background.setArcHeight(50);
            background.setFill(Color.WHITE);
            background.setStroke(Color.BLACK);
            background.setStrokeWidth(3);

            Circle trigger = new Circle(25);
            trigger.setCenterX(25);
            trigger.setCenterY(25);
            trigger.setFill(Color.WHITE);
            trigger.setStroke(Color.BLACK);
            trigger.setStrokeWidth(3);
            
            Text text2 = new Text("hello");
            text2.setX(trigger.getCenterX() - 15);
            text2.setY(trigger.getCenterY() +5);
            text2.textProperty().bind(Bindings.when(switchedOnProperty()).then("ON").otherwise("OFF"));

            DropShadow shadow = new DropShadow();
            
            shadow.setRadius(2);
            trigger.setEffect(shadow);

            translateAnimation.setNode(trigger);
            translateAnimation2.setNode(text2);
            fillAnimation.setShape(background);

            getChildren().addAll(background, trigger, text2);

            switchedOn.addListener((obs, oldState, newState) -> {
                boolean isOn = newState.booleanValue();
                translateAnimation.setToX(isOn ? 100 - 50 : 0);
                translateAnimation2.setToX(isOn ? 100 - 50 : 0);
                fillAnimation.setFromValue(isOn ? Color.WHITE : Color.BLUEVIOLET);
                fillAnimation.setToValue(isOn ? Color.BLUEVIOLET : Color.WHITE);

                animation.play();
            });

            setOnMouseClicked(event -> {
                switchedOn.set(!switchedOn.get());
            });
        }
    }

    
    
}