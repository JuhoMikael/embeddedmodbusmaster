package com.JuhoMikael.ModbusMasterEmbedded;

import java.net.ConnectException;
import java.net.InetAddress;

import net.wimpi.modbus.Modbus;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.ReadInputDiscretesRequest;
import net.wimpi.modbus.msg.ReadInputDiscretesResponse;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;
import net.wimpi.modbus.msg.WriteCoilRequest;
import net.wimpi.modbus.msg.WriteCoilResponse;
import net.wimpi.modbus.net.TCPMasterConnection;

public class modbusMasterEngine extends Thread {

	
	TCPMasterConnection con = null; 
	ModbusTCPTransaction trans = null; 
	ModbusTCPTransaction trans2 = null;
	ModbusTCPTransaction trans3 = null;
	ReadInputDiscretesRequest req = null; 
	ReadInputDiscretesResponse res = null; 
	ReadInputRegistersRequest req2 = null;
	ReadInputRegistersResponse res2 = null;
	WriteCoilRequest weq = null;
	WriteCoilResponse wes = null;
	WriteCoilRequest weq2 = null;
	WriteCoilResponse wes2 = null;

	
	InetAddress addr = null; //the slave's address
	int port = Modbus.DEFAULT_PORT;
	int ref = 0; 
	int ref2 = 0;
	int ref3 = 0;
	int count3 = 2;
	int count2 = 2;
	int count = 1; 
	int repeat = 100; 
	int rtu = 0;
	
	boolean DI1 = false;
	
	int IR1 = 0;
	int IR2 = 0;
	boolean DO1=false;
	boolean DO2=false;
	
	public boolean getDI1(){
		return DI1;
	}
	
	
	public int getIR1(){
		return IR1;
	}
	
	public int getIR2(){
		return IR2;
	}
	
	public void setDO1(boolean val){
		DO1 = val;
	}
	
	public void setDO2(boolean val2){
		DO2 = val2;
	}
	
	public void setSlaveInet(InetAddress a){
		addr = a;
	}
	
	public void setSlaveRTU(int a){
		rtu = a;
	}
	
	public void setRef(int r){
		ref = r;
	}
	
	public void setCount(int c){
		count = c;
	}
	public void run(){
	
	try {
	     
		
	   addr =InetAddress.getByName("192.168.11.64");
		
	    con = new TCPMasterConnection(addr);
	    con.setPort(port);
	    con.setTimeout(5000);
	    
	    System.out.println(addr);
	    System.out.println(port);
	    System.out.println(ref+ref2+count+count2);
	    System.out.println(repeat);
	    System.out.println("Is This Ok?");
	    
	    try{
	    con.connect();
	    }
	    	catch(ConnectException e){
	    		e.printStackTrace();
	    		System.out.println("CONNECTION FAIL");
	    	}


	    req = new ReadInputDiscretesRequest(ref, 3);
	    req2 = new ReadInputRegistersRequest(ref2, count2);
	    

	    trans = new ModbusTCPTransaction(con);
	    trans2 = new ModbusTCPTransaction(con);
	    
	    trans.setRequest(req);
	    trans2.setRequest(req2);
	    
	    
	    while(true) {
	      trans.execute();
	      res = (ReadInputDiscretesResponse) trans.getResponse();
	      trans2.execute();
	      res2 = (ReadInputRegistersResponse) trans2.getResponse();
	      
	      IR1=res2.getRegisterValue(0);
	      IR2=res2.getRegisterValue(1);
	      DI1 = res.getDiscreteStatus(2);

	      System.out.println("Digital Inputs Status=" + res.getDiscretes().toString() + "\n" + "Register Status: Temp:" + res2.getRegisterValue(0) + " - Hum:" + res2.getRegisterValue(1));
	      System.out.println("CONTROL SENT VAL:" +DO1);
	      if(DO1==true){
	    	weq = new WriteCoilRequest(ref3, true);
	  	    weq.setUnitID(21);
	  	    trans3 = new ModbusTCPTransaction(con);
		    trans3.setRequest(weq);

	    	trans3.execute();
	    	System.out.println(trans3.getRequest().getHexMessage());
	    	wes=(WriteCoilResponse) trans3.getResponse();
	    	System.out.println("Tulos: "+wes);
	    	System.out.println(trans3.getResponse().getHexMessage());
	    	Thread.sleep(1000);
	      }
	      
	      else if(DO2==true){
	    	weq2 = new WriteCoilRequest(ref3 +1, true);
	  	    weq2.setUnitID(21);
	  	    trans3 = new ModbusTCPTransaction(con);
		    trans3.setRequest(weq2);
	    	trans3.execute();
	    	System.out.println(trans3.getRequest().getHexMessage());
	    	wes2=(WriteCoilResponse) trans3.getResponse();
	    	System.out.println("Tulos: "+wes2);
	    	System.out.println(trans3.getResponse().getHexMessage());
	    	Thread.sleep(1000);
	      }
	      
	      else{
	    	  weq = new WriteCoilRequest(ref3, false);
	    	  weq = new WriteCoilRequest(ref3 +1, false);
		  	  weq.setUnitID(21);
		  	  trans3 = new ModbusTCPTransaction(con);
			  trans3.setRequest(weq);
			  trans3.execute();
	      } 
	    	  
	      try{
	      Thread.sleep(800);
	      }
	      catch(InterruptedException e){
	    	  System.out.println("SLEEP FAIL");
	    	  System.exit(0);
	      }
	      }
	      }
	   
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
}
}
