# README #


### What is this repository for? ###

* A JavaFX HMI & Modbus TCP master software for Raspberry Pi with 7" Touch screen.

* Version 1.0


### Who do I talk to? ###

* Repo owner or admin

## Pictures:

## Digital Inputs
![DI](img/di.JPG)

## Digital Outputs
![DO](img/do.JPG)

## Measurements
![INT](img/temperature.JPG)